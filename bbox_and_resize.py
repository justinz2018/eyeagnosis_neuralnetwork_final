import cv2
import glob, os
import numpy as np
import sys

def process(NAME):
    img = cv2.imread(NAME)
    #DIM = (min(img.shape[:2]),min(img.shape[:2]))
    
    '''AVG_SZ = 40
    GRAYSCALE = False
    REM_RATIO = .7
    
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    _,thresh = cv2.threshold(gray,10,255,cv2.THRESH_BINARY)
    _,o_contours,hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    x,y,w,h = cv2.boundingRect(max(o_contours, key=lambda x: cv2.contourArea(x)))
    if GRAYSCALE:
        crop = gray[y:y+h,x:x+w]
    else:
        crop = img[y:y+h,x:x+w]
    resized = cv2.resize(crop, DIM)
    if GRAYSCALE:
        _,thresh = cv2.threshold(resized,10,255,cv2.THRESH_BINARY)
    else:
        _,thresh = cv2.threshold(cv2.cvtColor(resized,cv2.COLOR_BGR2GRAY),10,255,cv2.THRESH_BINARY)
    _,o_contours,hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    eye = max(o_contours,key=lambda x: cv2.contourArea(x))

    resized -= cv2.filter2D(resized, -1, np.ones((AVG_SZ, AVG_SZ), np.float32)/(AVG_SZ*AVG_SZ))
    if GRAYSCALE:
        _,thresh = cv2.threshold(resized,250,255,cv2.THRESH_BINARY)
    else:
        _,thresh = cv2.threshold(cv2.cvtColor(resized,cv2.COLOR_BGR2GRAY),250,255,cv2.THRESH_BINARY)
    _,contours,hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    mask = np.ones(resized.shape[:2], dtype="uint8") * 255
    
    tot_area = sum([cv2.contourArea(c) for c in contours])
    
    tmp = sorted(contours, key=lambda x: cv2.contourArea(x))
    tar = REM_RATIO * tot_area
    rem = []
    area = 0
    i = 0
    for c in tmp:
        area += cv2.contourArea(c)
        if area > tar:
            break
        rem.append(c)
        i += 1
    if len(tmp) < i and abs(area+cv2.contourArea(tmp[i])-tar) < abs(area-tar):
        rem.append(tmp[i])
    cv2.drawContours(mask, [eye], -1, 0, -1)
    mask = 255-mask
    cv2.drawContours(mask, rem, -1, 0, 1)
    resized = cv2.bitwise_and(resized, resized, mask=mask)
    cv2.imwrite(NAME,resized)'''
    
    if img.shape[:2] == (1024, 1024): return 
    a = cv2.resize(img, (1024, 1024))
    cv2.imwrite(NAME, a)

if __name__ == "__main__":
    if len(sys.argv) >= 2:
        process(sys.argv[1])
        sys.exit(0)
    count = 0
    
    img_files = glob.glob("train/**/*.jpeg", recursive=True)
    img_files += glob.glob("validation/**/*.jpeg", recursive=True)
    img_files += glob.glob("test/**/*.jpeg", recursive=True)
    img_files += glob.glob("unused/**/*.jpeg", recursive=True)
    for i in img_files:
        basename = os.path.basename(i)
        fn, _ = os.path.splitext(basename)
        os.rename(i, "train/{0}".format(basename))
    
    for infile in glob.glob("train/*.jpeg"):
        process(infile)
        count += 1
        if count % 10 == 0:
            print(count)
