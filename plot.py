from glob import glob
import json

import matplotlib.pyplot as plt

files = sorted(glob("*.txt"))

d = {}
a = None
x = []
for f in files:
	x.append(int(f[0:f.find('.')]))
	d[f] = json.loads(open(f, "r").read().replace("'",'"'))
	a = d[f]

for k in a:
	vals = []
	for l in d:
		vals.append(d[l][k])
	plt.plot(x, vals)


