require("torch")
require("nn")
require("xlua")
require("optim")
require("cunn")
require("rnn")
require("cudnn")
local dl = require("dataload")

opt = {["datapath"]="../siemens/train", ["img_multiplier"]=8,
		["learningRate"]=.01, ["epochs"]=100000, ["cuda"]=true, ["batch_size"]=3,
		["cudnn"]=true, ["dropout"]=.2, ["print_every"]=5}

dataloader = dl.ImageClass(opt.datapath, {3, opt.img_multiplier*256, opt.img_multiplier*256})
model = nn.Sequential()

model:add(nn.SpatialConvolution(3, 7, 5, 5))
model:add(nn.ReLU())
model:add(nn.SpatialConvolution(7, 7, 5, 5))
model:add(nn.ReLU())
model:add(nn.Dropout(opt.dropout))
model:add(nn.SpatialMaxPooling(2, 2))

model:add(nn.SpatialConvolution(7, 6, 4, 4))
model:add(nn.ReLU())
model:add(nn.SpatialConvolution(6, 6, 4, 4))
model:add(nn.ReLU())
model:add(nn.Dropout(opt.dropout))
model:add(nn.SpatialMaxPooling(2, 2))

model:add(nn.SpatialConvolution(6, 6, 4, 4))
model:add(nn.ReLU())
model:add(nn.Dropout(opt.dropout))
model:add(nn.SpatialMaxPooling(2, 2))

model:add(nn.SpatialConvolution(6, 5, 4, 4))
model:add(nn.ReLU())
model:add(nn.Dropout(opt.dropout))
model:add(nn.SpatialMaxPooling(2, 2))

model:add(nn.SpatialConvolution(5, 5, 4, 4))
model:add(nn.ReLU())
model:add(nn.Dropout(opt.dropout))
model:add(nn.SpatialMaxPooling(2, 2))

model:add(nn.SpatialConvolution(5, 5, 4, 4))
model:add(nn.ReLU())
model:add(nn.Dropout(opt.dropout))
model:add(nn.SpatialMaxPooling(2, 2))

model:add(nn.SpatialConvolution(5, 5, 4, 4))
model:add(nn.ReLU())
model:add(nn.Dropout(opt.dropout))
model:add(nn.SpatialMaxPooling(2, 2))

model:add(nn.Reshape(5*12*12))
model:add(nn.Linear(5*12*12, 2))
model:add(nn.LogSoftMax())

local min1 = 1000000000
weights = torch.Tensor(#dataloader.classList)
for i=1,#dataloader.classList do
	min1 = math.min(min1, dataloader.classList[i]:size(1))
	weights[i] = 1/dataloader.classList[i]:size(1)
end
for i=1,#dataloader.classList do
	weights[i] = weights[i] * min1
end
crit = nn.ClassNLLCriterion(weights)

if opt.cuda then
	model:cuda()
	crit:cuda()
	if opt.cudnn then
		cudnn.convert(model, cudnn)
	end
end

param,gradParam = model:getParameters()

--dataloader = dl.TensorLoader(inputs, targets)
for i=1,opt.epochs do
	t = optim.ConfusionMatrix(#dataloader.classes)
	t:zero()
	local count = 0
	local loss_avg = 0
	for k, inputs, targets in dataloader:sampleiter(opt.batch_size) do
		count = count + 1
		inputs = inputs:double()
		-- random flip pls
		targets = targets:double()
		if opt.cuda then
			inputs = inputs:cuda()
			targets = targets:cuda()
		end
		local func = function(x)
			collectgarbage()
			param:copy(x)
			gradParam:zero()
			local out = model:forward(inputs)
			t:batchAdd(out, targets)
			local loss = crit:forward(out, targets)
			local df_dx = crit:backward(out, targets)
			model:backward(inputs, df_dx)
			
			loss_avg = loss_avg + loss
			
			xlua.progress(k, dataloader:size())
			if count % opt.print_every == 0 then
				print(t)
				print("LOSS "..loss/opt.print_every)
				loss_avg = 0
			end
			--io.write(" ", torch.cmul(t.mat:double(), torch.eye(#dataloader.classes)):sum() / t.mat:sum())

			return loss, gradParam
		end
		optim.adam(func, param, {
				learningRate = opt.learningRate,
				verbose = true
			 })
		io.write("\r")
	end
	torch.save(i..".model", model)
	print("END OF EPOCH "..i)
	print(t)
end
