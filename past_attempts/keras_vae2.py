import numpy as np
import matplotlib.pyplot as plt

from keras.layers import Input, Dense, Lambda, Flatten, Reshape
from keras.layers import Convolution2D, Deconvolution2D, MaxPooling2D
from keras.models import Model, load_model
from keras import backend as K
from keras import objectives
import random, glob, sys

from keras.layers import merge
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.models import Model, Sequential, load_model
from keras.layers import Input
from keras.optimizers import Adam
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras.regularizers import l2
import keras
import numpy as np
import matplotlib.pyplot as plt

train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"

batch_size = 16
WEIGHTS_FN = "weights_vae/{epoch:02d}-{val_loss:.2f}.hdf5"
RNG_SEED = 12345678
DROPOUT = .5
NB_EPOCH = 200 # rely on early stopping!
LR = .000033333
W_REG = .000000

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

af_ratio = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True)) / len(glob.glob(train_data_dir+"/1/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))

# input image dimensions
img_rows, img_cols, img_chns = 224, 224, 1
# number of convolutional filters to use
nb_filters = 32
# convolution kernel size
nb_conv = 3

original_dim = (img_chns, img_rows, img_cols)
latent_dim = 1000
intermediate_dim = 128
epsilon_std = 0.01
nb_epoch = 50

encoder = load_model("weights_vae/encoder.hdf5")
for l in encoder.layers:
	l.trainable = False
x = Input(batch_shape=(batch_size,) + original_dim)
z_mean = encoder(x)

d1 = Dense(1024)(z_mean)
d1 = Activation("tanh")(d1)
if DROPOUT:
	d1 = Dropout(DROPOUT)(d1)
d2 = Dense(512)(d1)
d2 = Activation("tanh")(d2)
if DROPOUT:
	d2 = Dropout(DROPOUT)(d2)
d3 = Dense(256)(d2)
d3 = Activation("tanh")(d3)
if DROPOUT:
	d3 = Dropout(DROPOUT)(d3)
d4 = Dense(1)(d3)
d4 = Activation("sigmoid")(d4)

def scaled_binary_crossentropy(y_true, y_pred):
	cost = binary_crossentropy(y_true, y_pred)
	mult = y_true*(af_ratio-1) + 1
	return cost * mult
	
def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))


#model = Model(x, d4)
#model.summary()
#model.compile(loss="binary_crossentropy",
#	  optimizer=Adam(lr=LR),
#	  metrics=["accuracy", f1_score])
model = load_model("weights_vae/47-0.69.hdf5", {"f1_score":f1_score})

train_datagen = ImageDataGenerator(
		horizontal_flip=True,
		vertical_flip=True,
		samplewise_center=True,
		#width_shift_range=100000,
		#height_shift_range=100000,
		samplewise_std_normalization=True,
		)
		
test_datagen = ImageDataGenerator(
		samplewise_center=True,
		samplewise_std_normalization=True)

train_generator = train_datagen.flow_from_directory(
		train_data_dir,
		target_size=(img_rows, img_cols),
		batch_size=batch_size,
		class_mode="binary",
		shuffle=True,
		color_mode="grayscale",
	#	save_to_dir="plots_own/test_img"
	   )
		
validation_generator = test_datagen.flow_from_directory(
		validation_data_dir,
		target_size=(img_rows, img_cols),
		batch_size=batch_size,
		class_mode="binary",
		color_mode="grayscale"
	   )

test_generator = test_datagen.flow_from_directory(
		test_data_dir,
		target_size=(img_rows, img_cols),
		batch_size=batch_size,
		class_mode="binary",
		color_mode="grayscale"
	 )

try:
	history = History()
	'''model.fit_generator(
		train_generator,
		samples_per_epoch=total_train,
		nb_epoch=NB_EPOCH,
		validation_data=validation_generator,
		nb_val_samples=total_val,
		callbacks=[
			ModelCheckpoint(WEIGHTS_FN),
			history
			]
		)'''
	c = 0
	for x,y in train_generator:
		c += 1
		if c % 50 == 0:
			print(x)
			print(model.predict(x))
except KeyboardInterrupt:
	pass
print(history.history)
f = open("plots_vae/history.txt", "w")
f.write(str(history.history))
f.close()
model.save_weights(WEIGHTS_FN)
try:
	val_loss, = plt.plot(history.history["val_loss"], "r-", label="Validation Loss")
	loss, = plt.plot(history.history["loss"], "b-", label="Training Loss")
	plt.title("Loss scores (scaled binary crossentropy)")
	plt.legend(handles=[val_loss, loss])
	plt.savefig("plots_vae/loss_fig.png")
	plt.close()
	val_f1, = plt.plot(history.history["val_f1_score"], "r-", label="Validation F1")
	f1, = plt.plot(history.history["f1_score"], "b-", label="Test F1")
	plt.title("F1 scores")
	plt.legend(handles=[val_f1, f1])
	plt.savefig("plots_vae/f1_fig.png")
	plt.close()
except:
	pass
scores = model.evaluate_generator(test_generator, val_samples=total_test)
print(scores)
