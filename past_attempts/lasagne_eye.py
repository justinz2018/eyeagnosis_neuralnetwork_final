import sys
import glob
import os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import lasagne
from lasagne.layers import *
from lasagne.nonlinearities import *
import theano
import theano.tensor as T
import numpy as np

np.random.seed(1337)

from nolearn.lasagne import BatchIterator
from nolearn.lasagne import visualize
from keras.preprocessing.image import ImageDataGenerator
from keras.datasets import mnist

inputs = T.tensor4("inputs")
targets = T.matrix("targets")

LAMBDA = 0.001
nb_epoch = 12
batch_size = 16
img_rows = img_cols = 256
train_data_dir = "train"
validation_data_dir = "validation"
test_data_dir = "test"
plots_dir = "plots"

network = InputLayer(shape=(None, 3, img_rows, img_cols), input_var=inputs)

network = Conv2DLayer(network, num_filters=32, filter_size=(8, 8),
										nonlinearity=LeakyRectify(.01))
network = Conv2DLayer(network, num_filters=32, filter_size=(5, 5),
										nonlinearity=LeakyRectify(.01))
network = MaxPool2DLayer(network, pool_size=(2, 2))
'''
network = Conv2DLayer(network, num_filters=32, filter_size=(5, 5),
										nonlinearity=LeakyRectify(.01))
network = Conv2DLayer(DropoutLayer(network, .5), num_filters=32, filter_size=(3, 3),
										nonlinearity=LeakyRectify(.01))
#network = MaxPool2DLayer(DropoutLayer(network, .1), pool_size=(2, 2))
'''
network = DenseLayer(DropoutLayer(network, .25), num_units=168, nonlinearity=rectify)
network = DenseLayer(DropoutLayer(network, .5), num_units=1, nonlinearity=sigmoid)

params = get_all_params(network, trainable=True)
predictions = get_output(network)
costs = lasagne.objectives.binary_crossentropy(predictions, targets)
cost = costs.mean()
cost += LAMBDA * lasagne.regularization.regularize_network_params(
	network, lasagne.regularization.l2)
updates = lasagne.updates.adadelta(cost, params)
train = theano.function(inputs=[inputs, targets],
						outputs=[cost, predictions],
						updates=updates, allow_input_downcast=True)
predict = theano.function(inputs=[inputs, targets],
						outputs=[cost, predictions], allow_input_downcast=True)

train_datagen = ImageDataGenerator(
#        samplewise_std_normalization=True,
#        samplewise_center=True,
#        shear_range=0.3,
 #       color_shift_range=234.0
#        rotation_range=180,
        horizontal_flip=True,
        vertical_flip=True,
        rescale=1/255)
        
test_datagen = ImageDataGenerator(
#        samplewise_std_normalization=True,
#        samplewise_center=True,
        rescale=1/255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_rows, img_cols),
        batch_size=batch_size,
        class_mode="binary",
    #    color_mode="grayscale"
       )
        
validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_rows, img_cols),
        batch_size=batch_size,
        class_mode="binary",
    #    color_mode="grayscale"
       )

test_generator = test_datagen.flow_from_directory(
        test_data_dir,
        target_size=(img_rows, img_cols),
        batch_size=batch_size,
        class_mode="binary",
    #    color_mode="grayscale"
     )

nb_train = len(list(glob.glob("train/**/*.jpeg", recursive=True)))
nb_validation = len(list(glob.glob("validation/**/*.jpeg", recursive=True)))
for e in range(nb_epoch):
	print("-------------------------------------")
	print("EPOCH {}/{}".format(e+1, nb_epoch))
	count = 0
	tot_cost = tot_acc = 0
	for x, y in train_generator:
		cost, predictions = train(x, y[:, np.newaxis])
		count += len(x)
		acc = np.mean((predictions>=.5) == y)
		sys.stdout.write("COST: %-18.15s ACC: %-15s %8s/%-8s \r" %
			(cost, acc, count, nb_train))
		sys.stdout.flush()
		tot_cost += cost*len(x)
		tot_acc += acc*len(x)
		if count >= nb_train: break
	sys.stdout.write("TRAIN COST: %-18.15s TRAIN ACC: %-15s %8s/%-8s \r" %
		(tot_cost/nb_train, tot_acc/nb_train, count, nb_train))
	sys.stdout.flush()
	print()
	tot_cost = tot_acc = count = 0
	for x, y in validation_generator:
		cost, predictions = predict(x, y[:, np.newaxis])
		tot_cost += cost*len(x)
		tot_acc += np.mean((predictions>=.5) == y)*len(x)
		count += len(x)
		if count >= nb_validation: break
	tot_cost /= nb_validation
	tot_acc /= nb_validation
	print("VAL COST: {}, VAL ACC: {}".format(tot_cost, tot_acc))
	visualize.plot_conv_weights(get_all_layers(network)[1]).savefig(plots_dir+"/1/{}.png".format(e+1))
	visualize.plot_conv_weights(get_all_layers(network)[2]).savefig(plots_dir+"/2/{}.png".format(e+1))
	plt.close()
