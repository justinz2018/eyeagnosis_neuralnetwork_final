from __future__ import print_function
import random, glob, sys, json, pickle

import keras
from keras.layers import merge
from sklearn.utils.class_weight import compute_class_weight
from keras.applications.resnet50 import ResNet50
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout, Lambda
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers import Input
from keras.optimizers import Adam, SGD
from keras.preprocessing.image import load_img, img_to_array
from keras.regularizers import l2
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History, LearningRateScheduler
import numpy as np
import matplotlib.pyplot as plt
import theano.tensor as T

START_FROM = 0

class LogHistory(keras.callbacks.Callback):
	def __init__(self, pickle_model=False):
		self.pickle_model = pickle_model
	def on_epoch_end(self, epoch, logs={}):
		f = open("plots_"+NAME+"/{}.history".format(epoch), "w")
		f.write(str(logs))
		f.close()
		if self.pickle_model:
			pickle.dump(self.model, open("weights_"+NAME+'/{}.p'.format(epoch), "wb"))
		
NAME = "resnet_768"

img_rows = img_cols = IMG_ROWS = IMG_COLS = 512
train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"
batch_size = 3
WEIGHTS_FN = "weights_"+NAME+"/{epoch:03d}-{val_loss:.2f}.hdf5"
RNG_SEED = 345987
NB_EPOCH = 100000 # rely on early stopping!

LR_SCHEDULE = {30: .00003333333333}
DROPOUT = .10
LR = 0.001
L2_REG = .00001
ALPHA = .01


def scheduler(epoch):
	global LR
	global LR_SCHEDULE
	try:
		f = open("LR.txt")
		t = f.read()
		f.close()
		if t.strip().startswith("STATUS"):
			print("LR Status; curr: {}, schedule: {}".format(LR, str(LR_SCHEDULE)))
		elif t.strip().startswith("EXEC"):
			code = t[4:].strip()
			print("attempt exec:")
			print(code)
			a = input("IS THIS OKAY? (y/N): ")
			if len(a) > 0 and (a[0] == 'Y' or a[0] == 'y'):
				print("executing...")
				exec(code)
				print("finished, no errors")
			else:
				print("did not execute")
		else:
			n = {}
			d = json.loads(t)
			for k in d:
				n[int(k)] = float(d[k])
			print("new schedule set: {}".format(str(n)))
			LR_SCHEDULE = n
	except:
		pass
	if epoch in LR_SCHEDULE:
		LR = LR_SCHEDULE[epoch]
	return LR

total_train_0 = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

def clip(inp):
	return K.clip(inp, 0, 1)
	
def normalize(img):
	img -= K.mean(img)
	img /= K.std(img)
	return img
def ninety(mat):
	T.set_subtensor(mat[:,:,0], K.transpose(mat[:,:,0][::-1]))
	T.set_subtensor(mat[:,:,1], K.transpose(mat[:,:,1][::-1]))
	T.set_subtensor(mat[:,:,2], K.transpose(mat[:,:,2][::-1]))
	return mat
def one_eighty(mat):
	return ninety(ninety(mat))
def neg_ninety(mat):
	T.set_subtensor(mat[:,:,0], K.transpose(mat[:,:,0])[::-1])
	T.set_subtensor(mat[:,:,1], K.transpose(mat[:,:,1])[::-1])
	T.set_subtensor(mat[:,:,2], K.transpose(mat[:,:,2])[::-1])
	return mat

def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))

def apply_resnet(x, resnet, f_dense, act):
	a = resnet(x)
	a = Flatten()(a)
	a = f_dense(a)
	a = act(a)
	return a

if __name__ == '__main__':
	if START_FROM <= 0:
		internal = ResNet50(include_top=False, input_shape=(IMG_ROWS,IMG_COLS,3))
		f_dense = Dense(1000, name='fc1000')
		act = LeakyReLU(ALPHA)

		inp2 = Input((IMG_ROWS, IMG_COLS,3))
		model = Lambda(normalize)(inp2)
		b = Lambda(ninety)(model)
		c = Lambda(one_eighty)(model)
		d = Lambda(neg_ninety)(model)
		a = apply_resnet(model, internal, f_dense, act)
		b = apply_resnet(b, internal, f_dense, act)
		c = apply_resnet(c, internal, f_dense, act)
		d = apply_resnet(d, internal, f_dense, act)
		if DROPOUT is not None:
			a = Dropout(DROPOUT)(a)
			b = Dropout(DROPOUT)(b)
			c = Dropout(DROPOUT)(c)
			d = Dropout(DROPOUT)(d)
		model = merge([a,b,c,d], mode="ave")
		model = Dense(1024)(model)
		model = LeakyReLU(ALPHA)(model)
		model = Dense(512)(model)
		model = LeakyReLU(ALPHA)(model)
		model = Dense(1)(model)
		model = Activation("sigmoid")(model)
		#model = LeakyReLU(ALPHA)(model)
		#model = Lambda(clip, output_shape=lambda x:x)(model)
		model = Model(inp2, model)
		if L2_REG is not None:
			print("L2: {}".format(L2_REG))
			for layer in model.layers:
				layer.W_regularizer = l2(L2_REG)
				layer.kernel_regularizer = l2(L2_REG)
		model.summary()
		
		model.compile(loss="binary_crossentropy",
			#	optimizer=SGD(lr=LR, momentum=.9, nesterov=True),
				optimizer=Adam(lr=LR, clipnorm=.1),
				metrics=["accuracy", f1_score])
	else:
		print("START FROM: {}".format(START_FROM))
		weights = glob.glob(WEIGHTS_FN.format(epoch=START_FROM-1, val_loss=999.99).replace("999.99","*"))[0]
		print("WEIGHTS: {}".format(weights))
		model = keras.models.load_model(weights, custom_objects={'T':T, 'f1_score':f1_score})
	train_datagen = ImageDataGenerator(
			horizontal_flip=True,
			vertical_flip=True)
			
	test_datagen = ImageDataGenerator()

	train_generator = train_datagen.flow_from_directory(
			train_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
			shuffle=True,
		   )
			
	validation_generator = test_datagen.flow_from_directory(
			validation_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		   )

	test_generator = test_datagen.flow_from_directory(
			test_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		 )
	total_train_1 = total_train-total_train_0
	cw = compute_class_weight("balanced", np.array([0,1]), np.array([0]*total_train_0 + [1]*total_train_1))
	cw = {0: cw[0], 1:cw[1]}
	print(cw)
	try:
		history = History()
		model.fit_generator(
			train_generator,
			samples_per_epoch=total_train,
			nb_epoch=NB_EPOCH,
			validation_data=validation_generator,
			nb_val_samples=total_val,
			callbacks=[
				LearningRateScheduler(scheduler),
				ModelCheckpoint(WEIGHTS_FN #, save_best_only=True
						),
				history,
				LogHistory(pickle_model=True),
				],
			class_weight=cw,
			initial_epoch=START_FROM)
	except KeyboardInterrupt:
		pass
	print(history.history)
	f = open("plots_"+NAME+"/history.txt", "w")
	f.write(str(history.history))
	f.close()
	model.save_weights(WEIGHTS_FN)
	try:
		val_loss, = plt.plot(history.history["val_loss"], "r-", label="Validation Loss")
		loss, = plt.plot(history.history["loss"], "b-", label="Training Loss")
		plt.title("Loss scores (scaled binary crossentropy)")
		plt.legend(handles=[val_loss, loss])
		plt.savefig("plots_"+NAME+"/loss_fig.png")
		plt.close()
		val_f1, = plt.plot(history.history["val_f1_score"], "r-", label="Validation F1")
		f1, = plt.plot(history.history["f1_score"], "b-", label="Test F1")
		plt.title("F1 scores")
		plt.legend(handles=[val_f1, f1])
		plt.savefig("plots_"+NAME+"/f1_fig.png")
		plt.close()
	except:
		pass
	y_pred = []
	y_true = []
	f = open("plots_"+NAME+"/eval_fn.txt", "w")
	f.write(str(test_generator.filenames))
	f.close()
	for x, y in test_generator:
		y_pred += model.predict_on_batch(x).tolist()
		y_true += y.tolist()
		if len(y_pred) >= total_test:
			break
	f = open("plots_"+NAME+"/final_eval_pred.txt", "w")
	f.write(str(y_pred))
	f.close()
	f = open("plots_"+NAME+"/final_eval_true.txt", "w")
	f.write(str(y_true))
	f.close()
	scores = model.evaluate_generator(test_generator, val_samples=total_test)
	print(scores)
