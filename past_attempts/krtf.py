from __future__ import print_function
import random, glob, sys, json, pickle

import keras
from keras.layers import merge
import tensorflow as tf
from keras.regularizers import l2
from sklearn.utils.class_weight import compute_class_weight
from convnets import *
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout, Lambda
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers import Input
from keras.optimizers import Adam, SGD
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History, LearningRateScheduler
import numpy as np
import matplotlib.pyplot as plt
# import theano.tensor as T

START_FROM = 153
PI = 3.14159265358979323846264338

class LogHistory(keras.callbacks.Callback):
	def __init__(self, pickle_model=False):
		self.pickle_model = pickle_model
	def on_epoch_end(self, epoch, logs={}):
		f = open("plots_"+NAME+"/{:03d}.history".format(epoch), "w")
		f.write(str(logs))
		f.close()
		m = self.model
		if self.pickle_model:
			f = open('weights_'+NAME+'/{:03d}.p'.format(epoch), "wb")
			pickle.dump(m, f)
			f.close()
		else:
			f = open("weights_"+NAME+"/{:03d}_opt.p".format(epoch), "wb")
			pickle.dump(m.optimizer, f)
			f.close()

NAME = "resnet_768"

img_rows = img_cols = IMG_ROWS = IMG_COLS = 512
train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"
batch_size = 4
WEIGHTS_FN = "weights_"+NAME+"/{epoch:03d}-{val_loss:.2f}.hdf5"
RNG_SEED = 12345
NB_EPOCH = 100000 # rely on early stopping!

LR_SCHEDULE = {10: 0.0003333333, 20: 0.001, 118: 0.00033333333}
#LR = 0.001
ALPHA = .01
# DROPOUT = .2  000-095
# L2_REG = .00001  000-095
# DROPOUT = .4 096-118
# L2_REG = .0001 096-105
# DROPOUT = .6 # 119-125
# L2_REG = .005 106-118
# L2_REG = .05 # 120-125


# -- reset, start at 61 --
# DROPOUT = .65 # 61-120
# L2_REG = .8 # 61-120
CHANGED_PARAMS = True
#DROPOUT = .8 # 121-152
#L2_REG = 2.4 # 121-152
LR = .00033333333
L2_REG = 5 # 153-
DROPOUT = .9 #153-


def scheduler(epoch):
	global LR
	global LR_SCHEDULE
	try:
		f = open("LR.txt")
		t = f.read()
		f.close()
		if t.strip().startswith("STATUS"):
			print("LR Status; curr: {}, schedule: {}".format(LR, str(LR_SCHEDULE)))
		elif t.strip().startswith("EXEC"):
			code = t[4:].strip()
			print("attempt exec:")
			print(code)
			a = input("IS THIS OKAY? (y/N): ")
			if len(a) > 0 and (a[0] == 'Y' or a[0] == 'y'):
				print("executing...")
				exec(code)
				print("finished, no errors")
			else:
				print("did not execute")
		else:
			n = {}
			d = json.loads(t)
			for k in d:
				n[int(k)] = float(d[k])
			print("new schedule set: {}".format(str(n)))
			LR_SCHEDULE = n
	except:
		pass
	if epoch in LR_SCHEDULE:
		LR = LR_SCHEDULE[epoch]
	return LR

total_train_0 = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

def clip(inp):
	return K.clip(inp, 0, 1)
	
def normalize(img):
	img -= K.mean(img)
	img /= K.std(img)
	return img
def ninety(mat):
	#T.set_subtensor(mat[0], K.transpose(mat[0][::-1]))
	#T.set_subtensor(mat[1], K.transpose(mat[1][::-1]))
	#T.set_subtensor(mat[2], K.transpose(mat[2][::-1]))
	'''print(K.int_shape(mat))
	print(K.int_shape(mat[:, :, :, 0]))

	print(K.int_shape(mat[:, :, :, 0][::-1]))
	print(K.int_shape(K.expand_dims(mat[:, :, :, 0][::-1], dim=3)))
	L = [K.expand_dims(K.transpose(mat[:, :, :, 0][::-1]), dim=3),\
		K.expand_dims(K.transpose(mat[:, :, :, 1][::-1]), dim=3),\
		K.expand_dims(K.transpose(mat[:, :, :, 2][::-1]), dim=3)]
	print(L)
	mat = merge(L, mode="concat", concat_axis=3)
	mat = K.permute_dimensions(mat, (2, 0, 1, 3))
	print(K.int_shape(mat))
	print("o rip")'''
	'''print(K.int_shape(mat))
	print(K.int_shape(mat[0][::-1]))
	L = [K.expand_dims(K.transpose(mat[0][::-1]), dim=0),
		K.expand_dims(K.transpose(mat[1][::-1]), dim=0),
		K.expand_dims(K.transpose(mat[2][::-1]), dim=0)]
	print(L)
	mat = merge(L, mode="concat", concat_axis=0)'''
	print(K.int_shape(mat))
	return tf.contrib.image.rotate(mat, PI/2)
def one_eighty(mat):
	return tf.contrib.image.rotate(mat, PI)
def neg_ninety(mat):
	#T.set_subtensor(mat[0], K.transpose(mat[0])[::-1])
	#T.set_subtensor(mat[1], K.transpose(mat[1])[::-1])
	#T.set_subtensor(mat[2], K.transpose(mat[2])[::-1])
	return tf.contrib.image.rotate(mat, 3*PI/2)

def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))

if __name__ == '__main__':
	if START_FROM <= 0 or True:
		internal = ResNet_50()
		if K.image_dim_ordering() == "tf":
			inp2 = Input((IMG_ROWS, IMG_COLS, 3))
		else:
			inp2 = Input((3, IMG_ROWS, IMG_COLS))
		model = Lambda(normalize)(inp2)
		b = Lambda(ninety)(model)
		c = Lambda(one_eighty)(model)
		d = Lambda(neg_ninety)(model)
		a = internal(model)
		b = internal(b)
		c = internal(c)
		d = internal(d)
		if DROPOUT is not None:
			a = Dropout(DROPOUT)(a)
			b = Dropout(DROPOUT)(b)
			c = Dropout(DROPOUT)(c)
			d = Dropout(DROPOUT)(d)
		model = merge([a,b,c,d], mode="ave")
		model = Dense(1024)(model)
		model = LeakyReLU(ALPHA)(model)
		
		if DROPOUT is not None:
			model = Dropout(DROPOUT)(model)
		
		model = Dense(512)(model)
		model = LeakyReLU(ALPHA)(model)
		model = Dense(1)(model)
		model = Activation("sigmoid")(model)
		#model = LeakyReLU(ALPHA)(model)
		#model = Lambda(clip, output_shape=lambda x:x)(model)
		model = Model(inp2, model)
		if L2_REG is not None:
			print("L2: {}".format(L2_REG))
			for layer in model.layers:
				layer.W_regularizer = l2(L2_REG)

		model.summary()
		
		if START_FROM > 0:
			weights = glob.glob(WEIGHTS_FN.format(epoch=START_FROM-1, val_loss=999.99).replace("999.99","*"))[0]
			print("LD WEIGHTS: {}".format(weights))
			model.load_weights(weights)
			# opt = pickle.load(open("weights_"+NAME+"/{:03d}_opt.p".format(START_FROM-1), "rb"))
			opt = Adam(lr=LR)
		else:
			opt = Adam(lr=LR)
		model.compile(loss="binary_crossentropy",
			#	optimizer=SGD(lr=LR, momentum=.9, nesterov=True),
				optimizer=opt,
				metrics=["accuracy"])
			
		# The Theano -> TensorFlow stuff
		from keras import backend as K
		from keras.utils.np_utils import convert_kernel
		import tensorflow as tf
		ops = []
		for layer in model.layers:
			if layer.__class__.__name__ in ['Convolution1D', 'Convolution2D', 'Convolution3D', 'AtrousConvolution2D']:
				original_w = K.get_value(layer.W)
				converted_w = convert_kernel(original_w)
				ops.append(tf.assign(layer.W, converted_w).op)
	else:
		print("START FROM: {}".format(START_FROM))
		weights = glob.glob(WEIGHTS_FN.format(epoch=START_FROM-1, val_loss=999.99).replace("999.99","*"))[0]
		print("WEIGHTS: {}".format(weights))
		model = keras.models.load_model(weights, custom_objects={'T':T, 'f1_score':f1_score, 'ninety':ninety, 'neg_ninety': neg_ninety})
		opt = pickle.load(open("weights_"+NAME+'/{}_opt.p'.format(START_FROM-1), "rb"))
		model.optimizer = opt
		if CHANGED_PARAMS:
			for layer in model.layers:
				if hasattr(layer, 'p'):
					layer.p = DROPOUT
					print(layer)
					print("Changed dropout to {}".format(layer.p))
				elif hasattr(layer, 'W_regularizer'):
					layer.W_regularizer = l2(L2_REG)
					print(layer)
					print("Changed l2 to {}".format(layer.W_regularizer))
		model.compile(loss="binary_crossentropy",
				optimizer=model.optimizer,
				metrics=["accuracy", f1_score])
	train_datagen = ImageDataGenerator(
			horizontal_flip=True,
			vertical_flip=True)
			
	test_datagen = ImageDataGenerator()

	train_generator = train_datagen.flow_from_directory(
			train_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
			shuffle=True,
		   )
			
	validation_generator = test_datagen.flow_from_directory(
			validation_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		   )

	test_generator = test_datagen.flow_from_directory(
			test_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
			shuffle=False
		 )
	total_train_1 = total_train-total_train_0
	cw = compute_class_weight("balanced", np.array([0,1]), np.array([0]*total_train_0 + [1]*total_train_1))
	cw = {0: cw[0], 1:cw[1]}
	print(cw)
	try:
		history = History()
		model.fit_generator(
			train_generator,
			samples_per_epoch=total_train,
			nb_epoch=NB_EPOCH,
			validation_data=validation_generator,
			nb_val_samples=total_val,
			callbacks=[
				LearningRateScheduler(scheduler),
				ModelCheckpoint(WEIGHTS_FN, save_best_only=False),
				history,
				LogHistory(pickle_model=False),
				],
			class_weight=cw,
			initial_epoch=START_FROM)
	except KeyboardInterrupt:
		pass
	print(history.history)
	f = open("plots_"+NAME+"/history.txt", "w")
	f.write(str(history.history))
	f.close()
	model.save_weights(WEIGHTS_FN)
	try:
		val_loss, = plt.plot(history.history["val_loss"], "r-", label="Validation Loss")
		loss, = plt.plot(history.history["loss"], "b-", label="Training Loss")
		plt.title("Loss scores (scaled binary crossentropy)")
		plt.legend(handles=[val_loss, loss])
		plt.savefig("plots_"+NAME+"/loss_fig.png")
		plt.close()
		val_f1, = plt.plot(history.history["val_f1_score"], "r-", label="Validation F1")
		f1, = plt.plot(history.history["f1_score"], "b-", label="Test F1")
		plt.title("F1 scores")
		plt.legend(handles=[val_f1, f1])
		plt.savefig("plots_"+NAME+"/f1_fig.png")
		plt.close()
	except:
		pass
	y_pred = []
	y_true = []
	f = open("plots_"+NAME+"/eval_fn.txt", "w")
	f.write(str(test_generator.filenames))
	f.close()
	for x, y in test_generator:
		y_pred += model.predict_on_batch(x).tolist()
		y_true += y.tolist()
		if len(y_pred) >= total_test:
			break
	f = open("plots_"+NAME+"/final_eval_pred.txt", "w")
	f.write(str(y_pred))
	f.close()
	f = open("plots_"+NAME+"/final_eval_true.txt", "w")
	f.write(str(y_true))
	f.close()
	scores = model.evaluate_generator(test_generator, val_samples=total_test)
	print(scores)
